https://github.com/drone-demos
https://github.com/drone-demos/drone-demo-postgres
https://github.com/drone-demos/drone-with-go

https://github.com/drone-demos/drone-go-selenium
https://hub.docker.com/r/selenium/standalone-chrome/
https://github.com/SeleniumHQ/docker-selenium

docker run -d -p 4444:4444 -v /dev/shm:/dev/shm selenium/standalone-chrome:3.141.59-dubnium

https://github.com/drone-demos/drone-with-python

PHP
- https://codeception.com/
- https://github.com/kanboard/kanboard/blob/master/.travis.yml

----------------------------------------------------------------------------------------------------

Drone with Gitlab
- https://github.com/indava/gitlab/ 

postgresql:
  image: sameersbn/postgresql:9.4-3
  environment:
    - DB_USER=gitlab
    - DB_PASS=password
    - DB_NAME=gitlabhq_production
  volumes:
    - /home/docker/gitlab/postgresql:/var/lib/postgresql

redis:
  image: sameersbn/redis:latest
  volumes:
    - /home/gitlab/redis:/var/lib/redis

gitlab:
  image: sameersbn/gitlab:8.9.5
  links:
    - redis:redisio
    - postgresql:postgresql
    - drone:drone.dev
  ports:
    - "10080:80"
    - "10443:443"
    - "2222:22"
  environment:
    - TZ=America/Mexico_City
    - GITLAB_TIMEZONE=Kolkata

    - GITLAB_SECRETS_DB_KEY_BASE=4BA

    - GITLAB_HOST=domain.com
    - GITLAB_PORT=443
    - GITLAB_SSH_PORT=2222
    - GITLAB_HTTPS=true

drone:
  image: drone/drone:0.4.2
  ports:
    - "10081:8000"
  volumes:
    - /var/run/docker.sock:/var/run/docker.sock
    - /home/gitlab/drone:/var/lib/drone
  environment:
    REMOTE_DRIVER: gitlab
    REMOTE_CONFIG: https://www.domain.com?client_id=XYZ&client_secret=ABC&open=true

.drone.yml
build:
  image: indava/drupal
  commands:
    - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    - php -r "if (hash_file('SHA384', 'composer-setup.php') === '...') {  } else { unlink('composer-setup.php'); } echo PHP_EOL;"
    - php composer-setup.php
    - php -r "unlink('composer-setup.php');"
    - php composer.phar install
    - php vendor/bin/codecept run 

----------------------------------------------------------------------------------------------------

https://github.com/drone/drone/issues/1035

---

Can remote workers be set up with a simple ssh login? What permissions does the user need to have?
> A remote worker requires a Docker daemon that is exposed over TCP with TLS. Since Drone communicates with the Docker deamon over TCP (using the remote REST API) it does not require access to the machine or any special permissions. You can read more here:
> http://readme.drone.io/setup/config/workers/

---

This is one of the primary reasons Drone deviates from the Travis format. 
When you configure a build in Drone you specify image instead of language. 
You can use any image in the Docker index as your build environment. See http://readme.drone.io/usage/build/images/

---

----------------------------------------------------------------------------------------------------
