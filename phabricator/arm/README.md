# Run Phabricator on ARM device

Architecture:  
- DB: Alpine with MariaDB  
- Web: Alpine with Nginx  
- PHP: Alpine with PHP7  

A super compact Docker Compose setup with images based on Alpine Linux.  
Alpine core image is 5 MB and has access to a package repository that is as complete as any other.  

# Why?
Phabricator is a pain to setup, especially in Docker.  
There is a lot of miscellaneous configuration needed and it requires multiple applications to run:  
- database  
- web server  
- php-fpm  
- 4 php daemons  

# Usage
Simply edit the ```docker-compose.yml``` file with the desired DB password and then call

```docker-compose up -d && docker-compose scale phd-task=2```

The setup issue ```Phabricator Daemons Are Not Running``` can be safely ignored as the container running the core Phabricator cannot see the daemons in the other containers.

To configure the rest of the Phabricator configuration, simply use the helper script ```config.sh```:

```
chmod +x config.sh
./config.sh set security.alternate-file-domain 'https://files.phabcdn.net/'
```