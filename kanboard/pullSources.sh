#!/bin/bash

set -ueo pipefail

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
BASE_DIRECTORY="$SCRIPTPATH"

if [[ -d "$BASE_DIRECTORY" && ! -z "$(ls -A $BASE_DIRECTORY)" ]]; then
    pushd "$BASE_DIRECTORY"
    
    echo "####################################################"
    echo "Updating Kanboard"
    git pull

    echo "####################################################"
    echo "Updating Registration plugin"
    pushd "plugins/Registration"
    git pull
    popd

    echo "####################################################"
    echo "Updating Timetable plugin"
    pushd "plugins/Timetable"
    git pull
    popd

    echo "####################################################"
    echo "Updating TimeTracker plugin"
    pushd "plugins/TimeTracker"
    git pull

    popd
    popd    
fi
