<?php

// Database driver: sqlite, mysql or postgres (sqlite by default)
define('DB_DRIVER', 'mysql');

// Mysql/Postgres username
define('DB_USERNAME', 'kanboard');

// Mysql/Postgres password
define('DB_PASSWORD', 'kanboard');

// Mysql/Postgres hostname
# How to find host IP? docker inspect 36301916551f | grep Gateway | head -1 | cut -d'"' -f4
define('DB_HOSTNAME', 'localhost');

// Mysql/Postgres database name
define('DB_NAME', 'kanboard');

define('DEBUG', true);
define('LOG_DRIVER', 'stderr');

define('API_AUTHENTICATION_TOKEN', 'test');
