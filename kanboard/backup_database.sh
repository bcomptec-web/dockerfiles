#!/usr/bin/env bash

set -uxo pipefail

# crontab
# echo '0 0 * * * /var/www/kanboard/backup_database.sh >> /var/www/kanboard/../backup.log 2>&1' >> /etc/config/crontab

# Backup
# https://dev.mysql.com/doc/refman/8.0/en/using-mysqldump.html
# https://dev.mysql.com/doc/refman/8.0/en/mysqldump.html
# https://mariadb.com/kb/en/library/mysqldump/
# https://mariadb.com/kb/en/library/making-backups-with-mysqldump/

# Restore
# https://mariadb.com/kb/en/library/restoring-data-from-dump-files/
# gunzip < [backupfile.sql.gz] | /mnt/ext/opt/mariadb/bin/mysql --user=kanboard --password=KanBoarD_2018 kanboard

if [ "$(id -u)" != "0" ]; then
        echo "You must be root."
        exit 1
fi

echo "Start datetime: $(date)"

TIMESTAMP=$(date --rfc-3339=date)
SOURCE_PATH="<SOURCE PATH>"
DESTINATION_PATH="<DESTINATION PATH>"
STORAGE_PATH="$SOURCE_PATH"
SQL_BACKUP_FILE="$STORAGE_PATH/Kanboard_$TIMESTAMP.sql.bz2"
BACKUP_DAYS=3

mysqldump="/usr/bin/mysqldump"
rsync="/usr/bin/rsync"


if [[ ! -d "$STORAGE_PATH" ]]; then
    mkdir -p "$STORAGE_PATH"
fi

if [[ ! -d "$DESTINATION_PATH" ]]; then
    mkdir "$DESTINATION_PATH"
fi

if [[ ! -f "$SQL_BACKUP_FILE" ]]; then
    echo "Backup Kanboard"
    time $mysqldump --single-transaction --extended-insert --databases kanboard | bzip2 > "$SQL_BACKUP_FILE"
    chmod 664 "$SQL_BACKUP_FILE"
fi

echo "Calculate space requirements for archive mode"
NEEDED_SPACE=$(du -k $STORAGE_PATH | cut -f1)

tmp=$(df /mnt/Backup | tail -1)
USED_SPACE=$(echo $tmp | awk '{ print $3 }' )
AVAILABLE_SPACE=$(echo $tmp | awk '{ print $4 }' )

REMAINING_SPACE=$(( $AVAILABLE_SPACE - $NEEDED_SPACE ))
echo -e "Used space before sync: $((USED_SPACE/1024)) MiB\nAvailable space before sync: $((AVAILABLE_SPACE/1024)) MiB\nNeeded space to sync: $((NEEDED_SPACE/1024)) MiB\nRemaining space after sync: $((REMAINING_SPACE/1024)) MiB\n"

if [[ $REMAINING_SPACE -gt 0 ]]; then
    echo "Syncing backups from $SOURCE_PATH to $DESTINATION_PATH"
    DRY_RUN= # --dry-run
    time $rsync --itemize-changes --timeout=60 --ipv4 --recursive --times  --no-perms --no-owner --no-group --progress --stats --human-readable --partial --partial-dir=.rsync-partial --delete --exclude FS_DATA $DRY_RUN "$SOURCE_PATH/" "$DESTINATION_PATH"
else
    echo "No space available on destination to sync backups!"
    exit 1
fi

echo "End datetime: $(date)"

exit 0
