#!/usr/bin/env bash

# docker tag 82b18be3fa54 gitea/gitea:v1.4.0-rc1
make DOCKER_TAG=v1.4.0-rc1 docker
