FROM bcomptec/arm32v6-alpine:latest
LABEL maintainer="Alex Butum <butum@bcomp-tec.com>"

ENV USER git
ENV GITEA_CUSTOM /data/gitea
ENV GODEBUG=netdns=go
ENV GITEA_VERSION="1.4.0-rc1"

RUN apk add --update bash

RUN apk --no-cache add \
    su-exec \
    ca-certificates \
    sqlite \
    git \
    linux-pam \
    s6 \
    curl \
    openssh \
    wget \
    tzdata && \
  rm -rf \
    /var/cache/apk/* && \
  addgroup \
    -S -g 1000 \
    git && \
  adduser \
    -S -H -D \
    -h /data/git \
    -s /bin/bash \
    -u 1000 \
    -G git \
    git && \
  echo "git:$(date +%s | sha256sum | base64 | head -c 32)" | chpasswd

COPY docker /

COPY gitea /app/gitea/gitea

# COPY docker/app.ini /data/gitea/conf/app.ini

EXPOSE 22 3000

VOLUME ["/data"]

ENTRYPOINT ["/usr/bin/entrypoint"]
CMD ["/bin/s6-svscan", "/etc/s6"]
