#Makefile related to docker

# HowTo
# As root - from the parent directory /home/ali/Projects/Go/src/code.gitea.io/gitea/ run: GOPATH=/home/ali/Projects/Go/ make docker

DOCKER_IMAGE ?= gitea/arm32v6-gitea
DOCKER_TAG ?= v1.4.0-rc1
DOCKER_REF := $(DOCKER_IMAGE):$(DOCKER_TAG)

.PHONY: docker_compile
docker_compile:
	# docker run -ti --rm -v $(CURDIR):/go/src/code.gitea.io/gitea -w /go/src/code.gitea.io/gitea -e TAGS="bindata $(TAGS)" bcomptec/arm32v6-golang:latest make clean generate build
	TAGS="bindata $(TAGS)" GOOS=linux GOARCH=arm GOARM=6 make clean generate build

.PHONY: docker_build_image
docker_build_image:
	docker build -t $(DOCKER_REF) \
		     -t ${DOCKER_IMAGE}:latest .

.PHONY: docker
docker: docker_compile docker_build_image

.PHONY: docker_test
docker_test:
	docker run -it --rm ${DOCKER_IMAGE}:latest /app/gitea/gitea --help

.PHONY: docker_save_image
docker_save_image:
	mkdir -p deploy
	docker save -o deploy/gitea_app.tar gitea/arm32v6-gitea:latest
	ls -sh deploy/gitea_app.tar

.PHONY: docker_copy_image
docker_copy_image:
	scp -r deploy/ nas:/share/CE_CACHEDEV1_DATA/Container/Images/Gitea

.PHONY: docker_load_image
docker_load_image:
	ssh nas 'find /share/CE_CACHEDEV1_DATA/Container/Images/Gitea -type f | while read image; do echo "Loading $$image $$(date)"; time /share/CE_CACHEDEV1_DATA/.qpkg/container-station/bin/docker load -i $$image; echo "Loaded $$image $$(date)"; done'
	ssh nas "bash -l -c 'docker images'"

.PHONY: docker_deploy
docker_deploy:
	ssh nas mkdir -p /share/CE_CACHEDEV1_DATA/Container/Dockerfiles/Gitea
	scp docker-compose.yml .env nas:/share/CE_CACHEDEV1_DATA/Container/Dockerfiles/Gitea

.PHONY: docker_run
docker_run: docker_deploy docker_do_run docker_monitor

.PHONY: docker_do_run
docker_do_run:
	ssh nas "bash -l -c 'pushd /share/CE_CACHEDEV1_DATA/Container/Dockerfiles/Gitea && docker-compose up -d'"

.PHONY: docker_stop
docker_stop:
	ssh nas "bash -l -c 'pushd /share/CE_CACHEDEV1_DATA/Container/Dockerfiles/Gitea/ && docker-compose stop'"

.PHONY: docker_abort
docker_abort: docker_stop
	ssh nas "bash -l -c 'pushd /share/CE_CACHEDEV1_DATA/Container/Dockerfiles/Gitea/ && docker-compose rm -fv'"
	# https://github.com/docker-library/postgres/issues/41 && https://github.com/docker-library/postgres/issues/203#issuecomment-255200501
	ssh nas "bash -l -c 'rm -rf /share/CE_CACHEDEV1_DATA/Container/Volumes/Gitea/db/var/lib/postgresql/data'"

.PHONY: docker_version
docker_version:
	ssh nas "bash -l -c 'docker version && docker-compose version && docker images'"

.PHONY: docker_monitor
docker_monitor:
	#ssh nas "bash -l -c 'docker exec -it mattermost_db_1 psql -U postgres -c \'\du\''"
	#docker exec -it mattermost_db_1 psql -U postgres -c '\l'
	ssh nas "bash -l -c 'pushd /share/CE_CACHEDEV1_DATA/Container/Dockerfiles/Gitea/ && docker-compose ps && docker-compose logs -f -t'"

.PHONY: docker_do_restart
docker_do_restart:
	ssh nas "bash -l -c 'pushd /share/CE_CACHEDEV1_DATA/Container/Dockerfiles/Gitea/ && docker-compose restart $(SERVICE)'"

.PHONY: docker_restart # make docker_restart SERVICE="app"
docker_restart: docker_do_restart docker_monitor
