#!/bin/sh

QEMU_ARM_VERSION="2.9.1"

echo "Register QEMU in the build agent"
docker run --rm --privileged multiarch/qemu-user-static:register --reset

if [[ ! -d tmp ]]; then
    mkdir tmp
    
    if [[ ! -f tmp/qemu-arm-static.tar.gz ]]; then
        pushd tmp &&
            curl -L -o qemu-arm-static.tar.gz https://github.com/multiarch/qemu-user-static/releases/download/v${QEMU_ARM_VERSION}/qemu-arm-static.tar.gz &&
            tar xzf qemu-arm-static.tar.gz && rm -rf qemu-arm-static.tar.gz
        popd
    fi
fi 
