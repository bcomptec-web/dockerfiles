#!/usr/bin/env

# Steps:
# 1. Download source code: https://github.com/webhippie/templater
# 2. Build
GOOS=linux GOARCH=arm GOARM=6 make clean build
