# Alpine Linux

Customized [Alpine Linux](http://alpinelinux.org) base Docker image based on [official Alpine](https://registry.hub.docker.com/_/alpine/).  

Optionally, you can enable a cron daemon, to get it running you need to provide the environment variable `CRON_ENABLED=true`.  
`s6` process needs to be started to get the cron daemon started automatically.  
