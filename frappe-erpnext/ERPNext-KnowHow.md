# [ERPNext](https://erpnext.com/)  

- License: GPLv3
- Tech: Python, [Frappe framework](https://frappe.io/frappe), JavaScript, MariaDB  
- [Wikipedia](https://en.wikipedia.org/wiki/ERPNext)
- [Code](https://github.com/frappe/erpnext)
- [Discussion Forum](https://discuss.erpnext.com/)
- [Getting Started with ERPNext](https://erpnext.org/get-started)
- [An open source ERP system built to self-implement](https://opensource.com/business/14/11/building-open-source-erp)
- https://quintagroup.com/cms/python/erpnext

## Comparison
Big difference when you compare it with other competitor softwares, ERPNext does not have separate community/paid editions so everything is available to everybody as part of fastest growing community of users and experts.  

## Documentation

### User Manual
- [User manual (English)](https://erpnext.com/docs/user/manual/en)  
- [Benutzerhandbuch (Deutsch)](https://erpnext.com/docs/user/manual/de)  

### Install
- [Bench](https://github.com/frappe/bench)
- [Using ERPNext Virtual Machines](https://erpnext.org/download)
- [Supported Versions](https://github.com/frappe/erpnext/wiki/Supported-Versions)

```
Version 10: End of 2020            branch: v10.x.x
Version 11: End of 2021            branch: master   
Version 12: Not yet released       branch: develop
```

### Docker
- [frappe_docker](https://github.com/frappe/frappe_docker)
- [Official Docker for frappe](https://discuss.erpnext.com/t/official-docker-for-frappe/27152)
- [ERPNext Docker Installation](https://blog.karmacomputing.co.uk/erpnext-docker-installation/)
- [Hitchhiker's guide to building this frappe_docker image](https://github.com/frappe/frappe_docker/wiki/Hitchhiker's-guide-to-building-this-frappe_docker-image)

```
git clone --depth 1 https://github.com/frappe/frappe_docker.git
cd frappe_docker
docker-compose up -d
./dbench -c root "cd /home/frappe && chown -R frappe:frappe ./*"
./dbench -c frappe "cd .. && bench init --frappe-branch=master frappe-bench --ignore-exist --skip-redis-config-generation && cd frappe-bench"
./dbench -c frappe "mv Procfile_docker Procfile && mv sites/common_site_config_docker.json sites/common_site_config.json && bench set-mariadb-host mariadb"
./dbench new-site site1.local
./dbench get-app erpnext https://github.com/frappe/erpnext --branch master
./dbench --site site1.local install-app erpnext
./dbench start

http://localhost:8000
User: Administrator
Password: Admin

cat frappe-bench/sites/common_site_config.json
{
 "admin_password": "admin", 
 "auto_update": false, 
 "background_workers": 1, 
 "db_host": "mariadb", 
 "file_watcher_port": 6787, 
 "frappe_user": "frappe", 
 "gunicorn_workers": 4, 
 "rebase_on_pull": false, 
 "redis_cache": "redis://redis-cache:13000", 
 "redis_queue": "redis://redis-queue:11000", 
 "redis_socketio": "redis://redis-socketio:12000", 
 "restart_supervisor_on_update": false, 
 "root_password": "123", 
 "serve_default_site": true, 
 "shallow_clone": true, 
 "socketio_port": 9000, 
 "update_bench_on_update": true, 
 "webserver_port": 8000
}
```

### General
- [ERPNext - Getting Started](https://www.youtube.com/watch?v=vKjHRzMEei0)  
- [What is an ERP system? And why should businesses use it?](https://erpnext.com/docs/user/manual/en/introduction)  
- [Do I Need An Erp?](https://erpnext.com/docs/user/manual/en/introduction/do-i-need-an-erp)  
- [Open Source](https://erpnext.com/docs/user/manual/en/introduction/open-source)  
- [The Champion](https://erpnext.com/docs/user/manual/en/introduction/the-champion)  
- [Implementation strategy](https://erpnext.com/docs/user/manual/en/introduction/implementation-strategy)  
- [Key workflows](https://erpnext.com/docs/user/manual/en/introduction/key-workflows)  
- [Concepts and Terms](https://erpnext.com/docs/user/manual/en/introduction/concepts-and-terms)  

### Videos
- [ERPNext Video Tutorials (YouTube playlist)](https://www.youtube.com/playlist?list=PL3lFfCEoMxvxfcQ526x0AME-yyWMo3JKT)  
- [ERPNext Video Tutorials (Website)](https://erpnext.com/docs/user/videos/learn)  
- [ERPNext Webinars](https://www.youtube.com/playlist?list=PL3lFfCEoMxvxfcQ526x0AME-yyWMo3JKT)  

### Modules

- [An introduction to ERPNext module development](https://anybox.fr/blog/introduction-to-erpnext-module-development)

#### [CRM](https://erpnext.com/docs/user/manual/en/CRM)  
- [ERPNext - Customer Relationship Management (CRM)](https://erpnext.com/docs/user/videos/learn/lead-to-quotation.html)  
- ERPNext helps you track business Opportunities from Leads and Customers, send them Quotations, and make confirmed Sales Orders.  
- The Customer Relationship Management (CRM) module helps maintain Leads, Opportunities, and Customers.  
- [ERPNext for Services](https://erpnext.com/services)  
CRM: Track and follow up with leads and opportunites and send out rule based emails and newsletters.  
- [I need only “CRM”, what do I do](https://discuss.erpnext.com/t/i-need-only-crm-what-do-i-do/10219)  
- Email  
    - [ERPNext - Email Account Setup](https://www.youtube.com/watch?v=ChsFbIuG06g)  
    - [ERPNext - Email Inbox](https://www.youtube.com/watch?v=KkKwtRwGvKw)  
    - [ERPNext - Setting up Email](https://www.youtube.com/watch?v=YFYe0DrB95o)  
    - [ERPNext - Newsletter](https://www.youtube.com/watch?v=muLKsCrrDRo)
- Reports  
    - [CRM Reports](https://erpnext.com/docs/user/manual/en/CRM/crm_reports)  
    - [ERPNext - Custom Report Builder](https://www.youtube.com/watch?v=TxJGUNarcQs)  
- Data Import  
    - [ERPNext - Data Import (Updated)](https://www.youtube.com/watch?v=SVEQ_Fq6018)  
    - [ERPNext - Importing data from spreadsheet file](https://www.youtube.com/watch?v=Ta2Xx3QoK3E)  

### [Concepts and Terms](https://erpnext.com/docs/user/manual/en/introduction/concepts-and-terms)

#### [Lead](https://erpnext.com/docs/user/manual/en/CRM/lead)  
When you send out the word that you are around and have something valuable to offer, people will come in to check out your product. These are your Leads.  

They are called Leads because they may lead you to a sale.  
Sales people usually work on leads by calling them, building a relationship and sending information about their products or services.  
It is important to track all this conversation to enable another person who may have to follow-up on that contact.  
The new person is then able to know the history of that particular Lead.  

Leads are the entities constituting a first contact.  

Leads can be created by a system users or by a web-user. When a lead is created minimal info (name,email) is entered and the lead is (default) linked to the active system user, the owner of the lead A user configurable drop list is used to classify Status of the lead (Open, Replied etc)  

A Lead is a potential Customer, someone who can give you business.  
A Customer is an organization or individual who has given you business before (and has an Account in your system).  
A Contact is a person who belongs to the Customer.  

A Lead can sometimes be an organization you are trying to make a deal with.  
In this case you can select "Lead is an Organization" and add as many contacts within this organization as you want.  
It is useful if you are establishing a relationship with several people within the same organization.  

A Lead can be converted to a Customer by selecting “Customer” from the Make dropdown.  
Once the Customer is created, the Lead becomes “Converted” and any further Opportunities from the same source can be created against this Customer.  

#### [Opportunity](https://erpnext.com/docs/user/manual/en/CRM/opportunity)  
When you know a Lead is looking for some products or services to buy, you can track that as an Opportunity.  
Also opportunity document helps user to collect the requirement of a customer/lead.  

An Opportunity can also come from an existing Customer.  

You can create multiple Opportunities against the same Lead.  
In Opportunity, apart from the Communication, you can also add the Items for which the Lead or Contact is looking for.  

Best Practice: Leads and Opportunities are often referred as your “Sales Pipeline” this is what you need to track if you want to be able to predict how much business you are going to get in the future.  
Its always a good idea to be able to track what is coming in order to adjust your resources.  

#### [Customer](https://erpnext.com/docs/user/manual/en/CRM/customer)
A customer, who is sometimes known as a client, buyer, or purchaser is the one who receives goods, services, products, or ideas, from a seller for a monetary consideration.  
A customer can also receive goods or services from a vendor or a supplier for other valuable considerations.  

#### Contacts and Addresses
Contacts and Addresses in ERPNext are stored separately so that you can attach multiple Contacts or Addresses to Customers and Suppliers.  

#### Quotation
Customer's request to price an item or service.  

#### Supplier quotation
In some businesses, users collect the rates from their supplier against the customer requirement and based on the supplier rates they prepare the quotation for the customer.  
With ERPNext, you can make a supplier quotation from the opportunity itself.  

#### [Accounts](https://erpnext.com/docs/user/manual/en/accounts)
In ERPNext, there is a separate Account record for each Customer, for each Company.  
When you create a new Customer, ERPNext will automatically create an Account Ledger for the Customer under “Accounts Receivable” in the Company set in the Customer record.  

By default, the system does not generate an account for every customer.   
All Customers can be booked in one account called Debitors.   
In order to manage a separate account for each customer, you have to first create the account under Accounts Receivable in the Chart of Accounts and then add it on the customer's form accounts table.  

Whether you have an accountant in your internal team OR you do it yourself OR you have chosen to outsource it, the financial accounting process is almost always at the center any business management system (aka an ERP system).  

In ERPNext, your accounting operations consists of 3 main transactions:  
- Sales Invoice: The bills that you raise to your Customers for the products or services you provide.  
- Purchase Invoice: Bills that your Suppliers give you for their products or services.  
- Journal Entries: For accounting entries, like payments, credit and other types.  

- [Introduction to Accounting using ERPNext](https://medium.com/@basawarajsavalagi/introduction-to-accounting-using-erpnext-e4ae5179465d)  
- [Chart of accounts](https://erpnext.com/docs/user/manual/en/accounts/chart-of-accounts.html)  

#### Blog
- [SaaS Vs Self-Hosted](https://frappe.io/blog/general/saas-vs-self-hosted)
- [ERPNext is GST Ready](https://erpnext.org/blog/erpnext-features/erpnext-is-gst-ready)
- [Understanding the ERPNext Ecosystem](https://frappe.io/blog/community/understanding-the-erpnext-ecosystem)
- [New Email Design in ERPNext](https://frappe.io/blog/erpnext-features/new-email-design-in-erpnext)
- [Sales and Silliness Part I](https://frappe.io/blog/general/sales-and-silliness-part-i)
- [Growth Above All?](https://frappe.io/growth-above-all)
- [Open Source Softwares are Safer](https://frappe.io/blog/technology/open-source-softwares-are-safer)
- [Sales in 21st Century](https://frappe.io/blog/inside-erpnext/sales-in-21st-century)
- [The Total Cost of Ownership](https://frappe.io/the-total-cost-of-ownership)
- [Integrate ERPNext with your Favourite Apps, with Minimal Effort](https://frappe.io/blog/erpnext-features/erpnext-integrations)
- [The Case of ERP Customization](https://frappe.io/blog/erpnext-implementation/the-case-of-customization)
- [ERPNext, a Perfect Fit for Electronics Distributors](https://frappe.io/blog/erpnext-features/erpnext-for-electronics-distributors)
- [ERPNext Implementation at MahaOnline Limited](https://erpnext.org/blog/community/erpnext-implementation-at-mahaonline-limited)
- [Making ERPNext Help Videos with Scripting ](https://erpnext.org/blog/technology/making-erpnext-help-videos-with-scripting)
- [How to Build A Sustainable ERPNext Business](https://frappe.io/blog/community/how-to-build-a-sustainable-erpnext-business)
- [Opentaps vs ERPNext](https://frappe.io/blog/open-source/opentaps-vs-erpnext)
- [We are on Cloud](https://frappe.io/blog/general/we-are-on-cloud)
- [Community Talks from ERPNext Conference 2017](https://erpnext.org/blog/announcements/community-talks-from-erpnext-conference-2017)