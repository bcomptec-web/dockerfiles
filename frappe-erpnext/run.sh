#!/usr/bin/env bash

set -euo pipefail

DEFAULT_INIT=false
INIT=${1:-DEFAULT_INIT}

docker-compose up -d

if [[ "x$INIT" = "xtrue" ]]; then
    echo "[1] Init"
    ./dbench -c root "cd /home/frappe && chown -R frappe:frappe ./*"
    ./dbench -c frappe "cd .. && bench init frappe-bench --frappe-branch=master --ignore-exist --skip-redis-config-generation && cd frappe-bench"
    ./dbench -c frappe "cp Procfile_docker Procfile && cp sites/common_site_config_docker.json sites/common_site_config.json && bench set-mariadb-host mariadb"
fi

echo "[2] New site"
./dbench new-site site1.local

echo "[3] Get ERPNext"
./dbench get-app erpnext https://github.com/frappe/erpnext --branch master

echo "[4] Install ERPNext"
./dbench --site site1.local install-app erpnext

echo "[5] Run"
./dbench start

# In case of setup failure
# ./dbench --site site1.local reinstall
# ./dbench --site site1.local install-app erpnext (if erpnext is not installed)
# ./dbench start
