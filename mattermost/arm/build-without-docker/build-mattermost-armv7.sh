#!/usr/bin/env bash

# Dependencies: Go, Node.js, Yarn
# sudo pacman -S go go-tools libpng12 nodejs npm yarn
# Inspired from: https://github.com/SmartHoneybee/ubiquitous-memory

set -xue

BUILD_DIR="$(pwd -L)"

GOV=1.9.3
GOSHA="a4da5f4c07dfda8194c4621611aeb7ceaab98af0b38bfb29e1be2ebb04c3556c  go${GOV}.linux-amd64.tar.gz"
export GOROOT="${HOME}/Software/Go/${GOV}/go"
export GOPATH="${HOME}/Projects/Go"
export PATH="${GOROOT}/bin:${GOPATH}/bin:${PATH}"
export SRCROOT="${GOPATH}/src/github.com/mattermost"
export GOOS=linux
export GOARCH=arm
export GOARM=7
export MATTERMOST_SERVER_VERSION=4.6.1

echo "Creating directory structure"
rm -rf "${SRCROOT}/mattermost-server"
rm -rf "${SRCROOT}/mattermost-webapp"
install -d "${GOPATH}" "${GOROOT}" "${SRCROOT}/mattermost-"{server,webapp}

if [[ ! -f go${GOV}.linux-amd64.tar.gz ]]; then
    echo "Downloading Go $GOV"
    wget -nc -q "https://storage.googleapis.com/golang/go${GOV}.linux-amd64.tar.gz"
    sha256sum --check <<< "${GOSHA}"
fi

if [[ ! -d ${HOME}/Software/Go/${GOV} ]]; then
    echo "Extracting Go $GOV"
    tar -C "${HOME}/Software/Go/${GOV}" -xzf "go${GOV}.linux-amd64.tar.gz"
fi

echo "Go $GOV at $(which go)"

if [[ ! -n "$(ls -A ${SRCROOT}/mattermost-server)" ]]; then
    echo "Downloading mattermost-server ${MATTERMOST_SERVER_VERSION}"
    git clone https://github.com/mattermost/mattermost-server ${SRCROOT}/mattermost-server
fi

pushd "${SRCROOT}/mattermost-server"
    git checkout tags/v$MATTERMOST_SERVER_VERSION
popd

if [[ ! -n "$(ls -A ${SRCROOT}/mattermost-webapp)" ]]; then
    echo "Downloading mattermost-webapp"
    git clone https://github.com/mattermost/mattermost-webapp.git ${SRCROOT}/mattermost-webapp
fi

pushd "${SRCROOT}/mattermost-webapp"
    git checkout tags/v$MATTERMOST_SERVER_VERSION
popd

echo "Building mattermost-webapp"
make -j$(nproc) build -C "${SRCROOT}/mattermost-webapp"

echo "Patching mattermost-server ${MATTERMOST_SERVER_VERSION}"
patch -d "${SRCROOT}/mattermost-server" -p1 < "${BUILD_DIR}/make.patch"

echo "Downloading mattermost-server dependencies"
go get -v -d github.com/mattermost/mattermost-server/...

echo "Building mattermost-server ${MATTERMOST_SERVER_VERSION}"
make -j$(nproc) config-reset build-linux package-linux -C "${SRCROOT}/mattermost-server" GO="GOOS=$GOOS GOARCH=$GOARCH GOARM=$GOARM $(which go)" BUILD_NUMBER="dev-arm-tag${MATTERMOST_SERVER_VERSION}"

echo "Creating mattermost-server ${MATTERMOST_SERVER_VERSION} artifact"
mv "${SRCROOT}/mattermost-server/dist/mattermost-team-linux-arm.tar.gz" "${BUILD_DIR}/mattermost-${MATTERMOST_SERVER_VERSION}-arm.tar.gz"
sha512sum "${BUILD_DIR}/mattermost-${MATTERMOST_SERVER_VERSION}-arm.tar.gz" | tee "${BUILD_DIR}/mattermost-${MATTERMOST_SERVER_VERSION}-arm.tar.gz.sha512sum"

echo "Resulting artifacts"
# file mattermost/bin/platform
file "${BUILD_DIR}/mattermost-${MATTERMOST_SERVER_VERSION}-arm.tar.gz"
file "${BUILD_DIR}/mattermost-${MATTERMOST_SERVER_VERSION}-arm.tar.gz.sha512sum"

sha512sum --check "${BUILD_DIR}/mattermost-${MATTERMOST_SERVER_VERSION}-arm.tar.gz.sha512sum"

# Deploy
# scp "${BUILD_DIR}/mattermost-${MATTERMOST_SERVER_VERSION}-arm.tar.gz" admin@192.168.5.100:/opt
# ssh admin@192.168.5.100 tar -C /opt xvzf "mattermost-${MATTERMOST_SERVER_VERSION}-arm.tar.gz"

echo "Success!"

exit 0
