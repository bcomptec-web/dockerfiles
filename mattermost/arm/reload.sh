#!/bin/sh

../get_qemu.sh

docker-compose down --rmi all
docker-compose build
docker-compose up -d
