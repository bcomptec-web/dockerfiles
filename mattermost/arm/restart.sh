#!/usr/bin/env bash

ssh nas "bash -l -c '/etc/init.d/mysqld.sh restart'"
ssh nas "bash -l -c 'docker-compose -f /share/CE_CACHEDEV1_DATA/Container/Dockerfiles/Mattermost/docker-compose-run.yml restart'"
ssh nas "bash -l -c 'pushd /share/CE_CACHEDEV1_DATA/Container/Dockerfiles/Gitea/ && docker-compose restart'"
