#!/bin/bash

# if wal backup is not enabled, use minimal wal logging to reduce disk space
# https://www.postgresql.org/docs/current/static/runtime-config-wal.html
: ${WAL_LEVEL:=minimal}
: ${ARCHIVE_MODE:=off}
: ${ARCHIVE_TIMEOUT:=60}

# db_1   | 2018-02-07 15:03:41.963 EET [1] FATAL:  WAL streaming (max_wal_senders > 0) requires wal_level "replica" or "logical"
: ${MAX_WAL_SENDERS:=0}

export WAL_LEVEL
export ARCHIVE_MODE
export ARCHIVE_TIMEOUT

# PGDATA is defined in upstream postgres dockerfile

# https://github.com/postgres/postgres/blob/REL_10_1/src/backend/utils/misc/postgresql.conf.sample
function update_conf () {
    if [ -f $PGDATA/postgresql.conf ]; then
        sed -i "s/wal_level =.*$/wal_level = $WAL_LEVEL/g" $PGDATA/postgresql.conf
        sed -i "s/archive_mode =.*$/archive_mode = $ARCHIVE_MODE/g" $PGDATA/postgresql.conf
        sed -i "s/archive_timeout =.*$/archive_timeout = $ARCHIVE_TIMEOUT/g" $PGDATA/postgresql.conf
        sed -i "s/max_wal_senders =.*$/max_wal_senders = $MAX_WAL_SENDERS/g" $PGDATA/postgresql.conf
    fi
}

if [ "${1:0:1}" = '-'  ]; then
    set -- postgres "$@"
fi

if [ "$1" = 'postgres'  ]; then
    VARS=(AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY WALE_S3_PREFIX AWS_REGION)

    for v in ${VARS[@]}; do
        if [ "${!v}" = "" ]; then
            echo "$v is required for Wal-E but not set. Skipping Wal-E setup."
            
            update_conf
          
            # Run the postgresql entrypoint
            . /docker-entrypoint.sh
            
            exit
        fi    
    done

    umask u=rwx,g=rx,o=
    mkdir -p /etc/wal-e.d/env

    for v in ${VARS[@]}; do
        echo "${!v}" > /etc/wal-e.d/env/$v
    done
    chown -R root:postgres /etc/wal-e.d

    WAL_LEVEL=logical
    ARCHIVE_MODE=on
    MAX_WAL_SENDERS=10

    update_conf
    
    # Run the postgresql entrypoint
    . /docker-entrypoint.sh
fi
