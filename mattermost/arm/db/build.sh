#!/bin/bash

mkdir tmp
cp -r ../tmp/* tmp
docker build -t mattermost/db .
rm -rf tmp

# docker run mattermost/db:10.1 psql --version
