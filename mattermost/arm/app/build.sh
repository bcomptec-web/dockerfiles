#!/usr/bin/env bash

set -uo pipefail

mkdir tmp
cp -r ../tmp/* tmp
docker build --build-arg edition=team -t mattermost/app .
rm -rf tmp
