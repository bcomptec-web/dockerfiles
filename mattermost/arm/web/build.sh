#!/bin/bash

mkdir tmp
cp -r ../tmp/* tmp
docker build -t mattermost/web .
rm -rf tmp
